# Introduction

This is a ruby bot made for discord. It can reply to some defined commands:

![rubybot](https://user-images.githubusercontent.com/4305276/94504466-35fc6400-01df-11eb-9e12-4fee9b6f2e86.png)

the environment is installed on a docker image for simplicity. it uses the cool tool youtube-dl to know how to download youtube videos and convert them to mp3.

#  Installation

Create your env file and add your bot token there

`cp example_env .env`

To build the docker image

`docker build -t chinozarate .`

If you get an error while downloading youtube files in the dockerized app you can adjust the folder permissions

```
chmod 1777 docker_tmp
``` 

# Running the app

To run start the bot

`docker-compose run chinozarate bundle exec ruby app.rb`

or 

```
docker-compose up -d
docker-compose exec chinozarate bundle exec ruby app.rb
```

# Debugging

This repo uses the ruby debug gem, so instead of using `binding.pry` you should use `binding.break` 

# Updating tools to download videos
To update youtube-dl

`pip install --upgrade youtube-dl`


# google drive

Post-install message from websocket-client-simple:
The development of this gem has moved to https://github.com/ruby-jp/websocket-client-simple.
Post-install message from google-api-client:
*******************************************************************************
The google-api-client gem is deprecated and will likely not be updated further.

Instead, please install the gem corresponding to the specific service to use.
For example, to use the Google Drive V3 client, install google-apis-drive_v3.
For more information, see the FAQ in the OVERVIEW.md file or the YARD docs.
*******************************************************************************
