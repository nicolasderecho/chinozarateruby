require_relative '../../event_for_voice_channel_responder'
module ChinoZarate

  class Hello < EventForVoiceChannelResponder

    def reply
      @application.play_audio(possible_audio_paths.sample)
    end

    def possible_audio_paths
      ['./assets/hello.mp3']
    end

    def self.can_handle?(discord_event)
      discord_event.content.text =~ /hola chino/i ||
      discord_event.content.text =~ /chino (hola|saluda)/i
    end

  end

end