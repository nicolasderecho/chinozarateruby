require_relative '../../event_for_voice_channel_responder'
module ChinoZarate

  class CounterStrikeVoice < EventForVoiceChannelResponder

    def reply
      @application.play_audio(possible_audio_paths.sample)
    end

    def possible_audio_paths
      ['./assets/cs_roger.wav', './assets/cs_go_go_go.wav', './assets/cs_fireinhole.wav']
    end

    def self.can_handle?(discord_event)
      discord_event.content.text =~ Regexp.new("chino sale counter") ||
      discord_event.content.text =~ Regexp.new("sale counter chino") ||
      discord_event.content.text =~ Regexp.new("chino sale cs") ||
      discord_event.content.text =~ Regexp.new("sale cs chino")
    end

  end

end