require_relative '../../event_for_voice_channel_responder'
module ChinoZarate

  class Insult < EventForVoiceChannelResponder

    def reply
      @application.play_audio(possible_audio_paths.sample)
    end

    def possible_audio_paths
      ['./assets/suck_my_dick.mp3', './assets/suck_my_dick2.mp3']
    end

    def self.can_handle?(discord_event)
      first_option = 'chupame un huevo'
      second_option = 'anda a cagar'
      discord_event.content.text =~ Regexp.new("chino #{first_option}") ||
      discord_event.content.text =~ Regexp.new("#{first_option} chino") ||
      discord_event.content.text =~ Regexp.new("chino #{second_option}") ||
      discord_event.content.text =~ Regexp.new("#{second_option} chino")
    end

  end

end