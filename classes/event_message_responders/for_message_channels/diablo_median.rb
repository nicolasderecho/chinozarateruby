require_relative '../../event_responder'
module ChinoZarate

  class DiabloMedian < EventResponder
    def reply
      @event.respond(%Q(
      - Web => https://www.median-xl.com/ \n- Documentación sobre temas del juego => https://docs.median-xl.com/ \n- Algunas guías para armar PJs => https://forum.median-xl.com/viewforum.php?f=40 ))
    end

    def self.can_handle?(discord_event)
      discord_event.content.text == '!diablomedian' || discord_event.content.text == '!diablomedianxl' || discord_event.content.text == '!diablosigma' ||
      discord_event.content.text =~ /(como es el tema|tirame la posta|como instalo) (con|del|el) (diablo|diablo ii)? median/i
    end

  end

end