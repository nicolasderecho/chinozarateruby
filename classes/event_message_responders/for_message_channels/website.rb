require_relative '../../event_responder'
module ChinoZarate

  class Website < EventResponder
    def reply
      @event.respond 'https://chinozarate.herokuapp.com'
    end

    def self.can_handle?(discord_event)
      discord_event.content.text == '!web' ||
      discord_event.content.text =~ /chino cual es tu (p(a|á)gina|web)\??/
    end

  end

end