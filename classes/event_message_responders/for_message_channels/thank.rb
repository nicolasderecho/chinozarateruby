require_relative '../../event_responder'
module ChinoZarate

  class Thank < EventResponder
    def reply
      @event.respond possible_answers.sample
    end

    def possible_answers
      [
          'De nada campeón! Vamos floresta!',
          "De nada #{@event.message.author.mention}!",
          'De nada guachín',
          'Que sea la última vez que me rompés las pelotas!',
          'Chupame un huevo'
      ]
    end

    def self.can_handle?(discord_event)
      discord_event.content.text =~ /gracias chino/i
    end

  end

end