require_relative '../../event_responder'
module ChinoZarate

  class DiabloLpf < EventResponder

    def reply
      @event.respond 'https://diablolpf.vercel.app/'
    end

    def self.can_handle?(discord_event)
      discord_event.content.text == '!diablolpf'
    end

  end

end