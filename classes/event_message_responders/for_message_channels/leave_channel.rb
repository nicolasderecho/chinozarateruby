require_relative '../../event_responder'

module ChinoZarate

  class LeaveChannel < EventResponder
    def reply
      @application.disconnect_from_voice_channel
    end

    def self.can_handle?(discord_event)
      discord_event.content.text =~ /chino (sali|salí|tomatelas|raja|rajá|raja de aca)/
    end

  end

end