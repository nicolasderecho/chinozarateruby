require_relative '../../event_responder'
module ChinoZarate

  class PhotosLpf < EventResponder
    def reply
      @event.respond 'https://mega.nz/#F!g0Ux1AbB!AltjAoJJZgZBYT5yxrZK7w'
    end

    def self.can_handle?(discord_event)
      discord_event.content.text == '!fotoslpf'
    end

  end

end