require_relative '../../event_responder'
module ChinoZarate

  class Beer < EventResponder
    def reply
      @event.respond 'En http://www.bettercall.com.ar/ podes comprar por internet y suelen tener buenos precios'
      @event.respond 'Otra opción sino es http://www.aonikenkgourmet.com.ar'
      @event.respond 'Ojo que también podés conseguir cerveza otro mundo desde su web => http://otromundo.com/ y esta muy buena'
    end

    def self.can_handle?(discord_event)
      discord_event.content.text == '!birra' || discord_event.content.text =~ /donde se consigue birra artesanal\?/ ||
      discord_event.content.text =~ /donde compro birra artesanal\?/
    end

  end

end