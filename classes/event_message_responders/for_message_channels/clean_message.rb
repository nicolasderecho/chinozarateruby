require_relative '../../event_responder'
module ChinoZarate

  class CleanMessage < EventResponder

    def reply
      @channel.history(100).each_with_index do |message, index|
        begin
          @channel.delete_message(message)
        rescue StandardError => e
          puts "can't remove message number #{index + 1}"  
        end
      end
    end

    def self.can_handle?(discord_event)
      discord_event.content.text == 'chino limpia el canal'
    end

  end

end
