require_relative '../../event_responder'
module ChinoZarate

  class Greeting < EventResponder
    def reply
      @event.respond possible_answers.sample
    end

    def possible_answers
      [
          'Aloja!! Aguante Allboys!!!',
          '¿Qué acelga?',
          '¿Todo bien?',
          '¿Todo tranca?'
      ]
    end

    def self.can_handle?(discord_event)
      discord_event.content.text =~ /hola chino/i ||
      discord_event.content.text =~ /como va chino\?/i
    end

  end

end