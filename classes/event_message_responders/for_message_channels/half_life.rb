require_relative '../../event_responder'
module ChinoZarate

  class HalfLife < EventResponder
    def reply
      @event.respond 'https://www.youtube.com/watch?v=JoV9o6b91Sc'
    end

    def self.can_handle?(discord_event)
      discord_event.content.text =~ /como (juego|gano) (a|al|el)? (half-life|hl|half life)\?/
    end

  end

end