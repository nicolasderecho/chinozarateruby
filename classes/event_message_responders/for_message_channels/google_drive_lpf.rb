require_relative '../../event_responder'
module ChinoZarate

  class GoogleDriveLpf < EventResponder
    def reply
      @event.respond 'https://drive.google.com/drive/folders/1yY2T2Ej2BP7TcgbZVWEFAR0v48IEMm34'
    end

    def self.can_handle?(discord_event)
      discord_event.content.text == '!carpetalpf' ||
      discord_event.content.text =~ /chino (tira|tirá|tirame|tirate|cual es) la carpeta de lpf\/??/
    end

  end

end