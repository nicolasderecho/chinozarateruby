require_relative '../../event_responder'
module ChinoZarate

  class CounterStrikeExplanation < EventResponder
    def reply
      @event.respond "Hay 2 formas. La primera es ingresando los comandos en la consola del server: https://ibb.co/J2bWGNG \nEs la más fácil aunque tiene la paja de que tenes que minimizar el juego un toque para meter los comandos."
      @event.respond "La segunda implica bajar la consola del juego mientras estamos jugando y tirar los comandos desde ahí. Para eso primero que nada tenemos que mirar cual es la contraseña de RCON del server: https://ibb.co/dDc6J4K"
      @event.respond "Una vez que sabemos esa contraseña lo que hacemos es conectarnos al server normalmente, abrimos la consola del juego y tipeamos lo siguiente:
        ```md
rcon_password PASSWORD```
        \nUna vez hecho esto ya podemos tirar los comandos desde la consola del juego poniendo el prefijo rcon antes de cada comando.\nPor Ejemplo si el password de RCON fuera 'lpf':
        ```rcon_password lpf
rcon mp_autoteambalance 0
rcon bot_add_t
rcon sv_restart 10```
Bue y así con cualquier comando que quisieras tirar.\nOjo con poner mal el password de RCON porque podés quedar banneado. Eso le pasó a Nicolás una vez.
      "
    end

    def self.can_handle?(discord_event)
      discord_event.content.text =~ /chino como configuro (un|el) server (de|del) (cs|cstrike|counter-strike|counter|counter strike)\??/
    end
  end

end