require_relative '../../event_responder'
module ChinoZarate

  class CommandList < EventResponder
    def reply
      text = list.inject('') do |string, (key, value)|
        "#{string}!#{key.to_s}: #{value} \n\n"
      end

      @event.respond text
    end

    def list
      {
        allboys:   'te dice donde podes escuchar los partidos de floresta',
        birra:     'te tira una pagina para comprar buena birra artesanal',
        cs:        'te tira comandos para configurar un server del Counter Strike 1.6',
        diablolpf: 'te tira la mejor pagina del diablo II del mundo entero, lejos pibe',
        fotoslpf:  'te tira la pagina de Mega donde estan guardadas las fotos de L.P.F',
        hangouts: 'te comparte un link de hangouts para unirse a una videollamada',
        juegoslpf: 'te tira la pagina de Mega donde estan guardados todos los juegos',
        web: 'te tira la página web del chino zarate',
        worms: 'te tira comandos útiles cuando intentas levantar una partida del worms'
      }
    end

    def self.can_handle?(discord_event)
      discord_event.content.text == '!comandos'
    end

  end

end