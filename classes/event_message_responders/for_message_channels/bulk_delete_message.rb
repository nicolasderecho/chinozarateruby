require_relative '../../event_responder'
module ChinoZarate

  class BulkDeleteMessage < EventResponder

    def reply
      begin
        @channel.prune(100)
      rescue StandardError => e
        puts "can't bulk delete messages"
      end
    end

    def self.can_handle?(discord_event)
      discord_event.content.text == 'chino borra los mensajes' ||
      discord_event.content.text == 'chino borra todos los mensajes'
    end

  end

end
