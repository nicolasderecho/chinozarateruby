require_relative '../../event_responder'

module ChinoZarate

  class DisconnectChannel < EventResponder
    def reply
      @application.disconnect_from_voice_channel
    end

    def self.can_handle?(discord_event)
      discord_event.content.text =~ /chino (sali|salí|raja|rajá)/
    end

  end

end