require_relative '../../event_responder'
module ChinoZarate

  class RadminVpn < EventResponder

    def reply
      @event.respond "```md
Red: Juegos L.P.F
Password: juegosLPF2019!
```"
    end

    def self.can_handle?(discord_event)
      discord_event.content.text == '!radmin' ||
      discord_event.content.text == '!radminvpn' ||
      discord_event.content.text == '!vpn'
    end

  end

end