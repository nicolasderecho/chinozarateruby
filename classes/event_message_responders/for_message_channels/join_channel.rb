require_relative '../../event_responder'

module ChinoZarate

  class JoinChannel < EventResponder
    def reply
      @application.connect_to_voice_channel
    end

    def self.can_handle?(discord_event)
      return false if ENV['ENVIRONMENT'] == 'production'
      discord_event.content.text == 'chino metete' ||
      discord_event.content.text == 'chino entra'  ||
      discord_event.content.text == 'chino entrá'
    end

  end

end