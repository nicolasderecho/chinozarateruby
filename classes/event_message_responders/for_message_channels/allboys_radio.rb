require_relative '../../event_responder'
module ChinoZarate

  class AllboysRadio < EventResponder
    def reply
      @event.respond 'La radio de allboys es AM 690 https://www.am690.com.ar/ y a veces también lo pasan por Allboys TV http://www.allboystv.com/'
    end

    def self.can_handle?(discord_event)
      discord_event.content.text == '!allboys' ||
      discord_event.content.text =~ /radio (de|del) (allboys|albo|blanco|floresta)\?/
    end

  end

end