require_relative '../../event_responder'
module ChinoZarate

  class Hangout < EventResponder

    def reply
      @event.respond 'https://hangouts.google.com/call/2-gwIV3lJORWae0o9UuqAAEE'
    end

    def self.can_handle?(discord_event)
      discord_event.content.text == '!hangouts' || discord_event.content.text == '!hg' ||
      discord_event.content.text =~ /chino (tirate|tira|tirá|armate) un (hangout|hg)/ ||
      discord_event.content.text =~ /chino (tirate|tira|tirá|armate) (hangout|hg)/
    end

  end

end