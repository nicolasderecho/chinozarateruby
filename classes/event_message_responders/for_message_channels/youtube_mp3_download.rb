require_relative '../../youtube_downloader'
require_relative '../../google/connector'
require_relative '../../event_responder'

module ChinoZarate

  class YoutubeMp3Download < EventResponder

    attr_reader :youtube_url

    def initialize(discord_event)
      super
      @youtube_url = @content.raw_text.split.find { |string| YoutubeDownloader.is_youtube_url?(string) } || ""
    end

    def reply
      downloader = YoutubeDownloader.new(@youtube_url)
      @event.respond "bancame un toque que lo bajo y convierto a mp3"
      audio_file = downloader.download_audio
      @event.send_file(File.open(audio_file.filepath, "r"), caption: "ahi ta #{@event.message.author.mention} \:point_down:")
    rescue InvalidYoutubeUrlError
      @event.respond "ehh no veo que me hayas pasado la URL. tenés que pasarme un link de Youtube válido."
    rescue RestClient::PayloadTooLarge
      handle_payload_too_large(audio_file)
    rescue Discordrb::Errors::UnknownError => e
      if e.message.match?(/request entity too large/i)
        handle_payload_too_large(audio_file)
      else
        handle_unknown_error(e)
      end
    rescue StandardError => e
      puts e.class.name
      puts e.message
      handle_unknown_error(e)
    end

    def upload_audio_to_google(audio_file)
      google_connector = Google::Connector.new
      filename = audio_file.filepath.split('/').last
      file_url = google_connector.upload_file(audio_file.filepath, filename)
      @event.respond "#{@event.message.author.mention} lo logré!!! bajalo que en 24hs lo borro: #{filename} => #{file_url}"
    rescue StandardError
      @event.respond "no che no hubo caso. a comerlaaaa."
    end

    private def handle_payload_too_large(audio_file)
      @event.respond "Uh el mp3 pesa más de 8mb y discord no me deja subirlo. Dejame ver si puedo subirlo a google drive. Puede que tarde un rato"
      upload_audio_to_google(audio_file)
    end

    private def handle_unknown_error(error)
      @event.respond "Upa esto exploto. Avisale a mi creador que este archivo tiro #{error.message}"
      raise(error)
    end


    def self.can_handle?(discord_event)
      discord_event.content.text.match?(/chino (baja|bajate|bajame|descarga) este tema/i) ||
      discord_event.content.text.match?(/chino (baja|bajate|bajame) esta (cancion|canción)/i)
    end

  end

end