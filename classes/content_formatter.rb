class ContentFormatter

  attr_reader :raw_text, :text
  def initialize(content = '')
    raise 'Content must be a String' unless content.is_a?(String)
    @raw_text = (content || '').freeze
    @text = @raw_text.downcase.strip.freeze
  end

end