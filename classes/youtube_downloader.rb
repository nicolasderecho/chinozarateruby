require 'open3'
require 'restclient'

class InvalidYoutubeUrlError < StandardError; end

class YoutubeAudioFile
  attr_reader :original_extension, :extension, :filepath, :download_info
  def initialize(download_info, extension:)
    @download_info = download_info
    @original_extension = File.extname(@download_info['_filename'])
    @extension = extension
    @filepath = @download_info['_filename'].gsub(@original_extension, ".#{@extension}")
  end
end

class YoutubeDownloader

  attr_reader :youtube_url

  def initialize(youtube_url)
    @youtube_url = youtube_url
    validate_youtube_url!
  end

  # youtube-downloader
  #"youtube-dl -f bestaudio --no-playlist --extract-audio --audio-format mp3 --audio-quality 0 --print-json -o '/tmp/%(fulltitle)s.%(ext)s' #{@youtube_url}"
  def download_audio
    response, stderr, status = Open3.capture3("yt-dlp -f 'ba' -x --audio-format mp3 --no-playlist --print-json -o '/tmp/%(title)s.%(ext)s' #{@youtube_url}")
    download_info = JSON.parse(response)
    YoutubeAudioFile.new(download_info, extension: 'mp3')
  end

  def self.youtube_regex
    /^(?:https?:\/\/)?(?:www\.)?youtu(?:\.be|be\.com)\/(?:watch\?v=)?([\w-]{10,})/
  end

  def self.is_youtube_url?(url)
    youtube_regex.match(url)
  end

  private

  def youtube_url_is_valid?
    @youtube_url && !@youtube_url.empty?
  end

  def validate_youtube_url!
    raise InvalidYoutubeUrlError.new('invalid youtube url') unless youtube_url_is_valid?
  end

end