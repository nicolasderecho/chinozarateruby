require_relative './event_message_responders/silent_responder'
require_relative './content_formatter'
require_relative './discord_event'

module ChinoZarate

  class DiscordResponder

    def initialize(discord_event)
      @discord_event = discord_event
      @content = @discord_event.content
      @event   = @discord_event.trigger
      @channel = @event.channel
      @application = @discord_event.application
    end

    def reply
      raise 'Subclass responsibility'
    end

    def self.is_enabled_for?(discord_event)
      true
    end

    def self.only_enabled_when_is_in_voice_channel?
      raise 'Subclass responsibility'
    end

    def self.can_handle?(discord_event)
      raise 'Subclass responsibility'
    end

    def self.responders_list
      DiscordResponder.instance_eval { @responders ||= [] }
    end

    def self.insert_responder(new_responder)
      DiscordResponder.instance_eval do
        @responders ||= []
        @responders.push(new_responder)
        @responders = @responders.sort_by { |responder| responder.only_enabled_when_is_in_voice_channel? ? 0 : 1 }
      end
    end

    def self.inherited(child_class)
      return if ['ChinoZarate::EventResponder', 'ChinoZarate::EventForVoiceChannelResponder'].include?(child_class.to_s)
      DiscordResponder.instance_eval do
        insert_responder(child_class)
        puts "Added #{child_class} to the list"
      end
    end

    def self.responder_for(discord_event)
      responder_class = responders_list.find { |responder| responder.is_enabled_for?(discord_event) && responder.can_handle?(discord_event) } || SilentEventResponder
      responder_class.new(discord_event)
    end

  end

end