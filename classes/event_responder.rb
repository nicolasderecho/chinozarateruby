require_relative './discord_responder'

module ChinoZarate

  class EventResponder < DiscordResponder

    def self.only_enabled_when_is_in_voice_channel?
      false
    end

  end

end