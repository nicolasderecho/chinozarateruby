require_relative 'authorization'
require_relative 'file_list'

class Google::Connector

  DISCORD_FOLDER = '1S5M5FhxZgscq3sOp0RT9qInEEMpvBKGz'
  TWENTY_FOUR_HOURS = 24 * 3600

  attr_reader :client

  def initialize
    @authorization = Google::Authorization.new
    @client = @authorization.authorized_client
  end

  #list files inside discord folder
  def discord_files(older_than: nil)
    Google::FileList.new(@client.list_files(q: files_query(older_than: older_than)))
  end

  def delete_file(file_id)
    @client.delete_file(file_id)
  rescue Google::Apis::ClientError
    puts "File #{file_id} was already deleted"
  end

  def cleanup_discord_files(older_than: TWENTY_FOUR_HOURS)
    discord_files(older_than: older_than).each { |discord_file| delete_file(discord_file.id) }
  end

  def upload_file(filepath, filename)
    google_file = Google::Apis::DriveV3::File.new(name: filename, parents: [DISCORD_FOLDER], mime_type: 'audio/mp3')
    file_upload = client.create_file(google_file, upload_source: filepath)
    "https://drive.google.com/open?id=#{file_upload.id}"
  end

  private

  def time_minus(seconds)
    (Time.now.utc - seconds).iso8601
  end

  def files_query(older_than: nil)
    query = "parents in '#{DISCORD_FOLDER}'"
    query = query + " and createdTime < '#{time_minus(older_than)}'" unless older_than.nil?
    query
  end

end