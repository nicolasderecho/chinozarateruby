class Google::FileList
  include Enumerable

  def initialize(file_list)
    @file_list = file_list || []
  end

  def inspect
    @file_list.inspect
  end

  def each(&block)
    @file_list.files.each(&block)
  end

end