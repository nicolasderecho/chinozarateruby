require_relative './event_message_responders/silent_responder'
require_relative './content_formatter'

module ChinoZarate

  class DiscordEvent

    attr_reader :content, :trigger, :application

    def initialize(trigger:, application:)
      @trigger = trigger
      @application = application
      @content = ContentFormatter.new(@trigger.content)
    end

  end

end