class Application

  def initialize(bot)
    @bot = bot
    add_message_listener
    add_disconnect_listener
  end

  def start!
    @bot.run
  end

  def connect_to_voice_channel(voice_channel_id = ENV['VOICE_CHANNEL_ID'])
    when_connected do
      voice_channel = @bot.channel(voice_channel_id)
      @voice_bot = @bot.voice(voice_channel) || @bot.voice_connect(voice_channel)
    end
  end

  def disconnect_from_voice_channel
    @voice_bot&.destroy
    @voice_bot = nil
  end

  def play_audio(path)
    @voice_bot&.play_file(path, '-af "highpass=f=200, lowpass=f=3000"') unless @voice_bot&.playing?
  end

  def is_connected_to_voice_channel?
    !!@voice_bot
  end

  private

  def add_message_listener
    @bot.message do |event|
      next if event.from_bot?
      discord_event = ChinoZarate::DiscordEvent.new(trigger: event, application: self)
      event_responder = ChinoZarate::EventResponder.responder_for(discord_event)
      event_responder.reply
    end
  end

  def when_connected
    if @bot.connected?
      yield
    else
      @bot.ready { yield }
    end
  end

  def add_disconnect_listener
    at_exit { @bot&.stop }
  end

end