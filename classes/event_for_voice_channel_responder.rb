require_relative './discord_responder'

module ChinoZarate

  class EventForVoiceChannelResponder < DiscordResponder

    def self.is_enabled_for?(discord_event)
      discord_event.application.is_connected_to_voice_channel?
    end

    def self.only_enabled_when_is_in_voice_channel?
      true
    end

  end

end