require 'debug'
require_relative '../classes/event_responder'
require_relative '../classes/event_for_voice_channel_responder'
require_relative '../classes/application'
Dir['./classes/event_message_responders/**/*.rb'].each {|file| require file }
Dir['./spec/support/*.rb'].each {|file| require file }