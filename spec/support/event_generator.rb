require_relative 'discord_event_stubber'

def sample_event(message, channel: nil, username: nil)
  DiscordEventStubber.new(content: message || 'sample', username: username, channel: channel)
end

def sample_application
  Application.new(FakeBot.new)
end

def discord_event_for(trigger_event, application = nil)
  application ||= sample_application
  ChinoZarate::DiscordEvent.new(trigger: trigger_event, application: application)
end