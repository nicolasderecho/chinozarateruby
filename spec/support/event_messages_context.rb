RSpec.shared_context :event_messages_context, :shared_context => :metadata do
  let(:event_content)   { raise('event_content variable must be defined') }
  let(:event_username)  { 'Marcelo Gallardo' }
  let(:event)           { DiscordEventStubber.new(content: event_content, username: event_username) }
  let(:application) { Application.new(FakeBot.new) }
  let(:discord_event) { ChinoZarate::DiscordEvent.new(trigger: event, application: application) }
  let(:event_responder) { ChinoZarate::EventResponder.responder_for(discord_event) }

  subject { event_responder.reply }

  def expect_response_to_match(regex)
    expect(event.responses.any? { |response| response =~ regex } ).to be_truthy
  end

  def expect_response_to_be_blank
    expect(event.responses).to be_empty
  end

  def expect_response_to_match_one_of_the_possible_answers(*possible_answers)
    text_response = event.responses.first
    expect(event.responses.length).to be(1)
    expect(possible_answers.any? { |regex| text_response =~ regex }).to be_truthy
  end

end