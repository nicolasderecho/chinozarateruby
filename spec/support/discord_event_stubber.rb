class DiscordEventStubber

  attr_reader :content, :responses, :channel
  def initialize(content:, username: 'Lalo Landa', channel: nil)
    @content   = content
    @username  = username
    @channel   = channel
    @responses = []
  end

  def respond(response_content)
    @responses.push(response_content)
  end

  def response
    @responses.first
  end

  def message
    message_stubber.new(author_stubber.new(@username, @username))
  end

  private

    def message_stubber
      Struct.new(:author)
    end

    def author_stubber
      Struct.new(:name, :mention)
    end

end