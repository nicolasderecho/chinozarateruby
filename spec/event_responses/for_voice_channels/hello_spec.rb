RSpec.describe ChinoZarate::EventResponder do
  include_context :event_messages_context

  shared_examples :voice_hello_responder do |match_command|
    describe "when the content matches '#{match_command}'" do
      let(:event_content) { match_command }

      it 'plays an audio' do
        allow_any_instance_of(Application).to receive(:is_connected_to_voice_channel?).and_return(true)
        expect(application).to receive(:play_audio)
        expect_any_instance_of(ChinoZarate::Greeting).not_to receive(:reply)
        subject
      end

    end
  end

  it_behaves_like :voice_hello_responder, 'hola chino'
  it_behaves_like :voice_hello_responder, 'chino saluda'
  it_behaves_like :voice_hello_responder, 'chino hola'
end