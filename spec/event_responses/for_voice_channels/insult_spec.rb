RSpec.describe ChinoZarate::EventResponder do
  include_context :event_messages_context

  shared_examples :voice_hello_responder do |match_command|
    describe "when the content matches '#{match_command}'" do
      let(:event_content) { match_command }

      it 'plays an audio' do
        allow_any_instance_of(Application).to receive(:is_connected_to_voice_channel?).and_return(true)
        expect(application).to receive(:play_audio)
        subject
      end

    end
  end

  it_behaves_like :voice_hello_responder, 'anda a cagar chino'
  it_behaves_like :voice_hello_responder, 'chino anda a cagar'
  it_behaves_like :voice_hello_responder, 'chino chupame un huevo'
  it_behaves_like :voice_hello_responder, 'chupame un huevo chino'
end