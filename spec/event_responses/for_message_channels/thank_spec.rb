RSpec.describe ChinoZarate::EventResponder do
  include_context :event_messages_context

  let(:possible_answers) do
    [
        /de nada campeón! Vamos floresta!/i,
        /de nada #{event_username}!/i,
        /de nada guachín/i,
        /que sea la última vez que me rompés las pelotas/i,
        /chupame un huevo/i
    ]
  end

  shared_examples :thank_responder do |match_command|
    describe "when the content matches a thank" do
      let(:event_content) { match_command }

      it 'responds with one of the possible thank responses' do
        subject
        expect_response_to_match_one_of_the_possible_answers(*possible_answers)
      end
    end
  end

  it_behaves_like :thank_responder, 'muchas gracias chino'

end