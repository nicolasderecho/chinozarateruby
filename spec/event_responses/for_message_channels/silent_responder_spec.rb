RSpec.describe ChinoZarate::EventResponder do
  include_context :event_messages_context

  describe "when the content doesn't match any responder" do
    let(:event_content) { '!zucutrule borombombon' }
    it 'applies a silent response' do
      subject
      expect_response_to_be_blank
    end
  end
end