RSpec.describe ChinoZarate::EventResponder do
  include_context :event_messages_context

  shared_examples :command_list_responder do |match_command|
    describe "when the content matches '#{match_command}'" do
      let(:event_content) { match_command }

      it 'responds with the available commands' do
        subject
        expect_response_to_match(/!allboys/)
        expect_response_to_match(/!birra/)
        expect_response_to_match(/!hangouts/)
        expect_response_to_match(/!diablolpf/)
        expect_response_to_match(/!juegoslpf/)
        expect_response_to_match(/!worms/)
        expect_response_to_match(/!web/)
      end

    end
  end

  it_behaves_like :command_list_responder, '!comandos'
end