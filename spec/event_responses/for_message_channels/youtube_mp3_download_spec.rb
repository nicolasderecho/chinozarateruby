require "discordrb"

RSpec.describe ChinoZarate::YoutubeMp3Download do
  let(:youtube_url)  { "https://www.youtube.com/watch?v=61FasQ6KQCI" }
  let(:first_event)  { sample_event("chino bajate este tema #{youtube_url}") }
  let(:second_event) { sample_event("chino bajate esta canción #{youtube_url}") }
  let(:third_event)  { sample_event("chino bajate esta cancion #{youtube_url}") }
  let(:fourth_event)  { sample_event("chino descarga este tema #{youtube_url}") }
  let(:not_youtube_event) { sample_event("chino tirate los comandos del counter") }
  let(:event_without_youtube_link) { sample_event("chino bajate esta cancion larar") }
  let(:with_youtube_url) { ChinoZarate::YoutubeMp3Download.new( discord_event_for(first_event) ) }
  let(:without_youtube_url) { ChinoZarate::YoutubeMp3Download.new( discord_event_for(event_without_youtube_link) ) }
  let(:application) { Application.new(FakeBot.new) }

  describe ".can_handle?" do

    let (:responder_class) { ChinoZarate::YoutubeMp3Download }

    it "returns 'true' when the message ask to download a youtube URL" do
      expect(responder_class.can_handle?(discord_event_for(first_event))).to be true
      expect(responder_class.can_handle?(discord_event_for(second_event))).to be true
      expect(responder_class.can_handle?(discord_event_for(third_event))).to be true
      expect(responder_class.can_handle?(discord_event_for(fourth_event))).to be true
    end

    it "returns 'false' when the message doesn't ask to download a youtube URL" do
      expect(responder_class.can_handle?(discord_event_for(not_youtube_event))).to be false
    end

  end

  describe "#youtube_url" do

    it "returns the youtube url when the message includes it" do
      expect(with_youtube_url.youtube_url).to eq(youtube_url)
    end

    it "returns an empty string when the message doesn't include it" do
      expect(without_youtube_url.youtube_url).to be_empty
    end

  end

  describe "#reply" do
    it "returns an explanatory message when the message doesn't include a valid youtube link" do
      without_youtube_url.reply
      expect(event_without_youtube_link.response).to match(/pasarme un link de Youtube/)
    end

    RSpec.shared_examples "upload failed" do
      it "replies with an explanatory message" do
        expect { with_youtube_url.reply }.not_to raise_exception(RestClient::PayloadTooLarge)
        expect(first_event.responses[1]).to match(/discord no me deja subirlo/)
      end

      it "tries to send it to google drive" do
        expect(with_youtube_url).to receive(:upload_audio_to_google)
        with_youtube_url.reply
      end

      context "and the google upload fails" do
        before { allow(Google::Connector).to receive(:new).and_raise(StandardError) }

        it "sends an explanatory message" do
          expect { with_youtube_url.reply }.not_to raise_exception
          expect(first_event.responses.last).to match(/no che no hubo caso/)
        end

      end
    end 

    context "when the discord client raises a 'PayloadTooLarge' exception" do
      before do
        allow_any_instance_of(YoutubeDownloader).to receive(:download_audio).and_raise(RestClient::PayloadTooLarge)
      end

      include_examples "upload failed"
    end

    context "when the discord client raises a 'PayloadTooLarge' exception" do
      before do
        allow_any_instance_of(YoutubeDownloader).to receive(:download_audio).and_raise(Discordrb::Errors::UnknownError.new("Request entity too large"))
      end

      include_examples "upload failed"
    end

  end

end