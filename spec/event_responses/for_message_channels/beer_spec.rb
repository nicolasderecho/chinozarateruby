RSpec.describe ChinoZarate::EventResponder do
  include_context :event_messages_context

  shared_examples :beer_responder do |match_command|
    describe "when the content matches '#{match_command}'" do
      let(:event_content) { match_command }

      it 'responds with craft beer websites' do
        subject
        expect_response_to_match(/www\.aonikenkgourmet\.com\.ar/)
        expect_response_to_match(/www\.bettercall\.com\.ar/)
        expect_response_to_match(/otromundo\.com/)
      end

    end
  end

  it_behaves_like :beer_responder, '!birra'
  it_behaves_like :beer_responder, 'chino donde se consigue birra artesanal?'
  it_behaves_like :beer_responder, 'chino donde compro birra artesanal?'
end