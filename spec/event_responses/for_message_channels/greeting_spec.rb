RSpec.describe ChinoZarate::EventResponder do
  include_context :event_messages_context

  let(:possible_answers) do
    [
        /Aguante Allboys/i,
        /acelga\?/i,
        /todo bien\?/i,
        /todo tranca\?/i
    ]
  end

  shared_examples :greeting_responder do |match_command|
    describe "when the content matches a greeting" do
      let(:event_content) { match_command }

      it 'responds with one of the possible greetings responses' do
        expect_any_instance_of(ChinoZarate::Hello).not_to receive(:reply)
        subject
        expect_response_to_match_one_of_the_possible_answers(*possible_answers)
      end
    end
  end

  it_behaves_like :greeting_responder, 'esssaa hola chino'
  it_behaves_like :greeting_responder, 'ey como va chino?'

end