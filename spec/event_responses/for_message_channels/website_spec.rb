RSpec.describe ChinoZarate::EventResponder do
  include_context :event_messages_context

  shared_examples :website_responder do |match_command|
    describe "when the content matches '#{match_command}'" do
      let(:event_content) { match_command }

      it 'responds with his own website' do
        subject
        expect_response_to_match(/chinozarate\.herokuapp\.com/)
      end

    end
  end

  it_behaves_like :website_responder, '!web'
  it_behaves_like :website_responder, 'chino cual es tu página?'
  it_behaves_like :website_responder, 'chino cual es tu pagina?'
  it_behaves_like :website_responder, 'chino cual es tu pagina?'
  it_behaves_like :website_responder, 'chino cual es tu web?'
end