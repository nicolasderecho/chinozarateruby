RSpec.describe ChinoZarate::EventResponder do
  include_context :event_messages_context

  shared_examples :cstrike_server_explanation_responder do |match_command|
    describe "when the content matches '#{match_command}'" do
      let(:event_content) { match_command }

      it 'responds with the counter strike explanation' do
        subject
        expect_response_to_match(/hay 2 formas/i)
      end

    end
  end

  it_behaves_like :cstrike_server_explanation_responder, 'chino como configuro un server del cs?'
  it_behaves_like :cstrike_server_explanation_responder, 'chino como configuro el server del cs?'
  it_behaves_like :cstrike_server_explanation_responder, 'chino como configuro un server del cstrike?'
  it_behaves_like :cstrike_server_explanation_responder, 'chino como configuro un server del counter?'
  it_behaves_like :cstrike_server_explanation_responder, 'chino como configuro un server del counter-strike?'
  it_behaves_like :cstrike_server_explanation_responder, 'chino como configuro un server del counter strike?'
end