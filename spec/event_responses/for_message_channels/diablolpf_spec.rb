RSpec.describe ChinoZarate::EventResponder do
  include_context :event_messages_context

  shared_examples :diablolpf_responder do |match_command|
    describe "when the content matches '#{match_command}'" do
      let(:event_content) { match_command }

      it 'responds with diablo L.P.F website' do
        subject
        expect_response_to_match(/diablolpf\.vercel\.app/)
      end

    end
  end

  it_behaves_like :diablolpf_responder, '!diablolpf'
end