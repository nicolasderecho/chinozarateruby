RSpec.describe ChinoZarate::EventResponder do
  include_context :event_messages_context

  shared_examples :gameslpf_responder do |match_command|
    describe "when the content matches '#{match_command}'" do
      let(:event_content) { match_command }

      it 'responds with Mega Games website' do
        subject
        expect_response_to_match(/mega\.nz\/#F!or41ESyb!H7_bZCtA4ubFm8qygjJk5Q/)
      end

    end
  end

  it_behaves_like :gameslpf_responder, '!juegoslpf'
end