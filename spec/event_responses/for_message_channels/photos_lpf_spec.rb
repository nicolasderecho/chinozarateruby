RSpec.describe ChinoZarate::EventResponder do
  include_context :event_messages_context

  shared_examples :photoslpf_responder do |match_command|
    describe "when the content matches '#{match_command}'" do
      let(:event_content) { match_command }

      it 'responds with Photos L.P.F website' do
        subject
        expect_response_to_match(/https:\/\/mega\.nz\/#F!g0Ux1AbB!AltjAoJJZgZBYT5yxrZK7w/)
      end

    end
  end

  it_behaves_like :photoslpf_responder, '!fotoslpf'
end