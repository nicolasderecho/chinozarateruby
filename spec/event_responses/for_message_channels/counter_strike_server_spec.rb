RSpec.describe ChinoZarate::EventResponder do
  include_context :event_messages_context

  shared_examples :cstrike_server_responder do |match_command|
    describe "when the content matches '#{match_command}'" do
      let(:event_content) { match_command }

      it 'responds with the counter strike server commands' do
        subject
        expect_response_to_match(/bot_difficulty/)
      end

    end
  end

  it_behaves_like :cstrike_server_responder, '!cs'
  it_behaves_like :cstrike_server_responder, 'chino cuales eran los comandos del server del cs?'
  it_behaves_like :cstrike_server_responder, 'chino cuales eran los comandos del server del counter?'
  it_behaves_like :cstrike_server_responder, 'chino cuales eran los comandos del server del counter-strike?'
  it_behaves_like :cstrike_server_responder, 'chino cuales eran los comandos del server del counter strike?'
  it_behaves_like :cstrike_server_responder, 'chino cuales eran los comandos del server del cstrike?'
  it_behaves_like :cstrike_server_responder, 'chino tirate los comandos del cs'
  it_behaves_like :cstrike_server_responder, 'chino tirate los comandos del counter'
  it_behaves_like :cstrike_server_responder, 'chino tirate los comandos del counter-strike'
  it_behaves_like :cstrike_server_responder, 'chino tirate los comandos del counter strike'
  it_behaves_like :cstrike_server_responder, 'chino tirate los comandos del cstrike'
  it_behaves_like :cstrike_server_responder, 'chino cuales son los comandos del server del counter?'
  it_behaves_like :cstrike_server_responder, 'chino tira los comandos del cstrike'
  it_behaves_like :cstrike_server_responder, 'chino tirame los comandos del cstrike'
  it_behaves_like :cstrike_server_responder, 'chino tirá los comandos del cstrike'
  it_behaves_like :cstrike_server_responder, 'chino tira los comandos de counter'
end