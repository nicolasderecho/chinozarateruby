RSpec.describe ChinoZarate::EventResponder do
  include_context :event_messages_context

  shared_examples :radmin_responder do |match_command|
    describe "when the content matches '#{match_command}'" do
      let(:event_content) { match_command }

      it 'responds with the radmin VPN credentials' do
        subject
        expect_response_to_match(/Juegos L\.P\.F/)
      end

    end
  end

  it_behaves_like :radmin_responder, '!radmin'
  it_behaves_like :radmin_responder, '!vpn'
  it_behaves_like :radmin_responder, '!radminvpn'
end