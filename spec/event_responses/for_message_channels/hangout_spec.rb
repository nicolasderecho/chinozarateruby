RSpec.describe ChinoZarate::EventResponder do
  include_context :event_messages_context

  shared_examples :hangouts_responder do |match_command|
    describe "when the content matches '#{match_command}'" do
      let(:event_content) { match_command }

      it 'responds with the hangouts link' do
        subject
        expect_response_to_match(/hangouts\.google\.com/)
      end

    end
  end

  it_behaves_like :hangouts_responder, '!hangouts'
  it_behaves_like :hangouts_responder, '!hg'
  it_behaves_like :hangouts_responder, 'chino tirate un hangout'
  it_behaves_like :hangouts_responder, 'chino tirate un hg'
  it_behaves_like :hangouts_responder, 'chino armate un hangout'
  it_behaves_like :hangouts_responder, 'chino armate un hg'
  it_behaves_like :hangouts_responder, 'chino tirate hangouts'
  it_behaves_like :hangouts_responder, 'chino tira hangouts'
  it_behaves_like :hangouts_responder, 'chino tira hangout'
  it_behaves_like :hangouts_responder, 'chino tirate hangouts'
end