RSpec.describe ChinoZarate::EventResponder do
  include_context :event_messages_context

  shared_examples :worms_responder do |match_command|
    describe "when the content matches '#{match_command}'" do
      let(:event_content) { match_command }

      it 'responds with his own website' do
        subject
        #https://worms2d.info/RubberWorm
        expect_response_to_match(/worms2d\.info/)
      end

    end
  end

  it_behaves_like :worms_responder, '!worms'
  it_behaves_like :worms_responder, 'chino tirate los comandos del worms'
  it_behaves_like :worms_responder, 'chino tirá los comandos del worms'
  it_behaves_like :worms_responder, 'chino tira los comandos del worms'
end