RSpec.describe ChinoZarate::EventResponder do
  include_context :event_messages_context

  shared_examples :allboys_responder do |match_command|
    describe "when the content matches '#{match_command}'" do
      let(:event_content) { match_command }

      it 'responds with the allboys page' do
        subject
        expect_response_to_match(/youtube.com\/watch\?v=JoV9o6b91Sc/)
      end

    end
  end

  it_behaves_like :allboys_responder, 'chino como juego al half life?'
  it_behaves_like :allboys_responder, 'chino como juego al hl?'
  it_behaves_like :allboys_responder, 'chino como juego al half-life?'
end