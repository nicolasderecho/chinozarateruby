RSpec.describe ChinoZarate::EventResponder do
  include_context :event_messages_context

  shared_examples :join_channel_responder do |match_command|
    describe "when the content matches '#{match_command}'" do
      let(:event_content) { match_command }

      it 'joins the bot into the voice channel' do
        expect(application).to receive(:connect_to_voice_channel)
        subject
      end

    end
  end

  it_behaves_like :join_channel_responder, 'chino metete'
  it_behaves_like :join_channel_responder, 'chino entra'
  it_behaves_like :join_channel_responder, 'chino entrá'
end