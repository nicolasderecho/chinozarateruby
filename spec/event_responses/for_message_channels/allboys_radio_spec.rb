RSpec.describe ChinoZarate::EventResponder do
  include_context :event_messages_context

  shared_examples :allboys_responder do |match_command|
    describe "when the content matches '#{match_command}'" do
      let(:event_content) { match_command }

      it 'responds with the allboys page' do
        subject
        expect_response_to_match(/allboystv\.com/)
        expect_response_to_match(/am690\.com\.ar/)
      end

    end
  end

  it_behaves_like :allboys_responder, '!allboys'
  it_behaves_like :allboys_responder, 'che chino cual era la radio de allboys?'
  it_behaves_like :allboys_responder, 'chino, cual era la radio de allboys?'
  it_behaves_like :allboys_responder, 'oh mi gran chino, cual era la radio del albo?'
  it_behaves_like :allboys_responder, 'se acuerdan cual era la radio del blanco?'
  it_behaves_like :allboys_responder, 'saben cual es la radio de allboys?'
end