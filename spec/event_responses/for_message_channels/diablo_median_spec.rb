RSpec.describe ChinoZarate::EventResponder do
  include_context :event_messages_context

  shared_examples :diablo_median_responder do |match_command|
    describe "when the content matches '#{match_command}'" do
      let(:event_content) { match_command }

      it 'responds with Diablo Median XL' do
        subject
        expect_response_to_match( /docs\.median-xl\.com\//)
      end

    end
  end

  it_behaves_like :diablo_median_responder, '!diablomedian'
  it_behaves_like :diablo_median_responder, '!diablomedianxl'
  it_behaves_like :diablo_median_responder, '!diablosigma'
end