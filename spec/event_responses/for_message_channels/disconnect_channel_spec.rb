RSpec.describe ChinoZarate::EventResponder do
  include_context :event_messages_context

  shared_examples :disconnect_channel_responder do |match_command|
    describe "when the content matches '#{match_command}'" do
      let(:event_content) { match_command }

      it 'joins the bot into the voice channel' do
        expect(application).to receive(:disconnect_from_voice_channel)
        subject
      end

    end
  end

  it_behaves_like :disconnect_channel_responder, 'chino sali'
  it_behaves_like :disconnect_channel_responder, 'chino salí'
  it_behaves_like :disconnect_channel_responder, 'chino rajá'
  it_behaves_like :disconnect_channel_responder, 'chino raja'
end