RSpec.describe ChinoZarate::EventResponder do
  include_context :event_messages_context

  shared_examples :google_drive_lpf_responder do |match_command|
    describe "when the content matches '#{match_command}'" do
      let(:event_content) { match_command }

      it 'responds with L.P.F shared folder in google drive' do
        subject
        expect_response_to_match(/https:\/\/drive\.google\.com\/drive\/folders\/1yY2T2Ej2BP7TcgbZVWEFAR0v48IEMm34/)
      end

    end
  end

  it_behaves_like :google_drive_lpf_responder, '!carpetalpf'
  it_behaves_like :google_drive_lpf_responder, 'chino cual es la carpeta de lpf?'
  it_behaves_like :google_drive_lpf_responder, 'chino tirame la carpeta de lpf'
  it_behaves_like :google_drive_lpf_responder, 'chino tirate la carpeta de lpf'
  it_behaves_like :google_drive_lpf_responder, 'chino tira la carpeta de lpf'
  it_behaves_like :google_drive_lpf_responder, 'chino tirá la carpeta de lpf'
end