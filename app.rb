require 'discordrb'
require 'discordrb/webhooks'
require 'dotenv'
require_relative './classes/event_responder'
require_relative './classes/application'
require_relative './classes/google/connector'
require 'rufus-scheduler'

Dotenv.load('.env')

require 'debug' if ENV['ENVIRONMENT'] == 'development'

def pputs(*args)
 puts(*args) if ENV['ENVIRONMENT'] == 'production'
end

Dir['./classes/event_message_responders/**/*.rb'].each {|file| require file }

google_connector = Google::Connector.new
scheduler = Rufus::Scheduler.new

scheduler.every ENV['SCHEDULER_RECURRENCY'] do
  pputs 'cleaning up discord files'
  google_connector.cleanup_discord_files
  pputs 'DONE'
end

bot = Discordrb::Bot.new(token: ENV['BOT_TOKEN'])

application = Application.new(bot)

application.start!