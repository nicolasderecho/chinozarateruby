FROM nicolasderecho/discord_ruby:3.3.4

RUN mkdir /bot
WORKDIR /bot
ADD Gemfile /bot/Gemfile 
ADD Gemfile.lock /bot/Gemfile.lock
RUN bundle install
ADD . /bot
CMD bundle exec ruby app.rb